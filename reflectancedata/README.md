# Notes on Data Set Usage #

The training records for the various reflectivity profiles of asteroids surveyed from earth were sourced as follows.

Source: [NASA's Planetary Data System](http://sbn.psi.edu/pds/)

Aggregate Spectrophotometry Data Used For Training:
- [24-Color Asteroid Survey](http://sbn.psi.edu/pds/resource/24color.html)
- [52-Color Asteroid Survey](http://sbn.psi.edu/pds/resource/52color.html)